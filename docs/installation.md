# How to Install

1. Navigate to XAMPP and clone to htdocs directory
2. Navigate to newly created directory, open the terminal and run: npm run install
    - run: npm run serve (if you encounter linting errors)
3. Start apache and mysql on xampp
4. Check app using the given development address